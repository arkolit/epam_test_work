package ru.belyaev.dto;

import lombok.Data;
import ru.belyaev.entities.Product;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by arkolit on 20.08.17.
 */
@Data
public class ProductInfoDto {

    private long id;
    private String name;
    private double cost;
    private DiscountDto discount;

    public static ProductInfoDto convertEntityToDto(Product product) {
        ProductInfoDto productDto = new ProductInfoDto();
        productDto.setId(product.getId());
        productDto.setName(product.getName());
        productDto.setCost(product.getCost());
        if (!product.getDiscounts().isEmpty()) {
            productDto.setDiscount(DiscountDto.convertEntityToDto(product.getDiscounts().iterator().next()));
        }
        return productDto;
    }

    public static Collection<ProductInfoDto> convertEntityToDto(Collection<Product> products) {
        Collection<ProductInfoDto> productsWithSales = new ArrayList<>();
        for (Product product : products) {
            productsWithSales.add(convertEntityToDto(product));
        }
        return productsWithSales;
    }

}
