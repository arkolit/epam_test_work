package ru.belyaev.dto;

import lombok.Data;
import ru.belyaev.entities.Discount;
import ru.belyaev.entities.Product;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by arkolit on 24.08.17.
 */
@Data
public class DiscountInfoDto {
    private long id;
    private Date dateDiscount;
    private int amount;
    private ProductDto product;
    private boolean isActive;

    public static DiscountInfoDto convertEntityToDto(Discount discount) {
        DiscountInfoDto discountInfoDto = new DiscountInfoDto();
        discountInfoDto.setId(discount.getId());
        discountInfoDto.setDateDiscount(discount.getDateDiscount());
        discountInfoDto.setAmount(discount.getAmount());
        discountInfoDto.setProduct(ProductDto.convertEntityToDto(discount.getProduct()));
        discountInfoDto.setActive(discount.isActive());

        return discountInfoDto;
    }


    public static Collection<DiscountInfoDto> convertEntityToDto(Collection<Discount> discounts) {
        Collection<DiscountInfoDto> discountDtos = new ArrayList<>();
        for (Discount discount : discounts) {
            discountDtos.add(convertEntityToDto(discount));
        }
        return discountDtos;
    }
}
