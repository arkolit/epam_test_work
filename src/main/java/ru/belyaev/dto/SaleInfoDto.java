package ru.belyaev.dto;

import lombok.Data;
import ru.belyaev.entities.Discount;
import ru.belyaev.entities.Product;
import ru.belyaev.entities.Sale;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import static java.lang.Math.round;

/**
 * Created by arkolit on 24.08.17.
 */
@Data
public class SaleInfoDto {
    private long id;
    private ProductDto product;
    private Date dateSale;
    private int quantity;
    private DiscountDto discount;
    private double summaryCost;

    public static SaleInfoDto convertEntityToDto(Sale sale) {
        SaleInfoDto saleInfoDto = new SaleInfoDto();
        saleInfoDto.setId(sale.getId());
        saleInfoDto.setQuantity(sale.getQuantity());
        saleInfoDto.setDateSale(sale.getDateSale());
        saleInfoDto.setProduct(ProductDto.convertEntityToDto(sale.getProduct()));
        double summary = saleInfoDto.getQuantity() * saleInfoDto.getProduct().getCost();
        if (sale.getDiscount() != null) {
            saleInfoDto.setDiscount(DiscountDto.convertEntityToDto(sale.getDiscount()));
            summary -= (saleInfoDto.getDiscount().getAmount() / 100.0) * summary;
        }
        saleInfoDto.setSummaryCost(Math.round(summary * 100.0) / 100.0);

        return saleInfoDto;
    }


    public static Collection<SaleInfoDto> convertEntityToDto(Collection<Sale> sales) {
        Collection<SaleInfoDto> saleInfoDtos = new ArrayList<>();
        for (Sale sale : sales) {
            saleInfoDtos.add(convertEntityToDto(sale));
        }
        return saleInfoDtos;
    }
}
