package ru.belyaev.dto;

import lombok.Data;
import ru.belyaev.entities.Discount;
import ru.belyaev.entities.Sale;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by arkolit on 24.08.17.
 */

@Data
public class DiscountDto {
    private long id;
    private Date dateDiscount;
    private int amount;
    private boolean isActive;

    public static DiscountDto convertEntityToDto(Discount discount) {
        DiscountDto discountDto = new DiscountDto();
        discountDto.setId(discount.getId());
        discountDto.setDateDiscount(discount.getDateDiscount());
        discountDto.setAmount(discount.getAmount());
        discountDto.setActive(discount.isActive());

        return discountDto;
    }


    public static Collection<DiscountDto> convertEntityToDto(Collection<Discount> discounts) {
        Collection<DiscountDto> discountDtos = new ArrayList<>();
        for (Discount discount : discounts) {
            discountDtos.add(convertEntityToDto(discount));
        }
        return discountDtos;
    }
}
