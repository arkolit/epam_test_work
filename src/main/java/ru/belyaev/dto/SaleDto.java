package ru.belyaev.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.belyaev.entities.Sale;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by arkolit on 23.08.17.
 */
@Data
public class SaleDto {
    private long id;
    private Date dateSale;
    private int quantity;
    private ProductDto product;
    private DiscountDto discount;

    public static SaleDto convertEntityToDto(Sale sale) {
        SaleDto saleDto = new SaleDto();
        saleDto.setId(sale.getId());
        saleDto.setDateSale(sale.getDateSale());
        saleDto.setQuantity(sale.getQuantity());
        saleDto.setProduct(ProductDto.convertEntityToDto(sale.getProduct()));
        if (sale.getDiscount() != null) {
            saleDto.setDiscount(DiscountDto.convertEntityToDto(sale.getDiscount()));
        }
        return saleDto;
    }

    public static Collection<SaleDto> convertEntityToDto(Collection<Sale> sales) {
        Collection<SaleDto> salesDto = new ArrayList<>();
        for (Sale sale : sales) {
            salesDto.add(convertEntityToDto(sale));
        }
        return salesDto;
    }
}
