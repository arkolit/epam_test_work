package ru.belyaev.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.belyaev.entities.Discount;
import ru.belyaev.entities.Product;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by arkolit on 23.08.17.
 */
@Data
public class ProductDto {
    private long id;
    private String name;
    private double cost;

    public static ProductDto convertEntityToDto(Product product) {
        ProductDto productDto = new ProductDto();
        productDto.setId(product.getId());
        productDto.setName(product.getName());
        productDto.setCost(product.getCost());
        return productDto;
    }

    public static Collection<ProductDto> convertEntityToDto(Collection<Product> products) {
        Collection<ProductDto> productsWithSales = new ArrayList<>();
        for (Product product : products) {
            productsWithSales.add(convertEntityToDto(product));
        }
        return productsWithSales;
    }
}
