package ru.belyaev.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.belyaev.dto.SaleDto;
import ru.belyaev.entities.Sale;
import ru.belyaev.repositories.ProductRepo;
import ru.belyaev.repositories.SaleRepo;

import java.util.Collection;

/**
 * Created by arkolit on 15.08.17.
 */
@RestController
@RequestMapping("/sales")
public class SaleRestController {

    private final SaleRepo saleRepo;

    @Autowired
    public SaleRestController(SaleRepo saleRepo) {
        this.saleRepo = saleRepo;
    }

    @RequestMapping(method = RequestMethod.GET)
    public Collection<SaleDto> readSales()
    {
        return SaleDto.convertEntityToDto((Collection<Sale>) saleRepo.findAll());
    }

    @RequestMapping(value = "/{saleId}", method = RequestMethod.GET)
    public SaleDto readSale(@PathVariable long saleId) {
        return SaleDto.convertEntityToDto(saleRepo.findOne(saleId));
    }
}
