package ru.belyaev.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.belyaev.dto.ProductDto;
import ru.belyaev.dto.ProductInfoDto;
import ru.belyaev.dto.SaleDto;
import ru.belyaev.entities.Product;
import ru.belyaev.entities.Sale;
import ru.belyaev.repositories.DiscountRepo;
import ru.belyaev.repositories.ProductRepo;
import ru.belyaev.repositories.SaleRepo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by arkolit on 14.08.17.
 */
@RestController
@RequestMapping("/products")
public class ProductRestController {

    private final ProductRepo productRepo;

    private final SaleRepo saleRepo;

    private final DiscountRepo discountRepo;

    @Autowired
    public ProductRestController(ProductRepo productRepo, SaleRepo saleRepo, DiscountRepo discountRepo) {
        this.productRepo = productRepo;
        this.saleRepo = saleRepo;
        this.discountRepo = discountRepo;
    }

    @RequestMapping(method = RequestMethod.GET)
    public Collection<ProductDto> readProducts()
    {
        return ProductDto.convertEntityToDto((Collection<Product>) productRepo.findAll());
    }

    @RequestMapping(value = "/{productId}", method = RequestMethod.GET)
    public ProductInfoDto readProduct(@PathVariable long productId) {
        return ProductInfoDto.convertEntityToDto(productRepo.findOne(productId));
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<ProductDto> addProduct (@RequestBody ProductDto productDto) {
        Product result = productRepo.save(new Product(productDto.getName(), productDto.getCost()));
        return new ResponseEntity<>(ProductDto.convertEntityToDto(result), HttpStatus.OK);
    }

    @RequestMapping(value = "/{productId}", method = RequestMethod.PUT)
    public ResponseEntity<ProductDto> updateProduct(@PathVariable long productId, @RequestBody ProductDto productDto) {
        Product product = productRepo.findOne(productId);

        if (product == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        product.setName(productDto.getName());
        product.setCost(productDto.getCost());
        productRepo.save(product);
        return new ResponseEntity<>(ProductDto.convertEntityToDto(product), HttpStatus.OK);
    }

    @RequestMapping(value = "/{productId}", method = RequestMethod.DELETE)
    public ResponseEntity<ProductDto> deleteProduct(@PathVariable long productId) {
        Product product = productRepo.findOne(productId);

        if (product == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        productRepo.delete(productId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/{productId}/sale", method = RequestMethod.POST)
    public ResponseEntity<SaleDto> addSales (@PathVariable long productId, @RequestBody SaleDto saleDto) {
        Product product = productRepo.findOne(productId);

        if (product == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Sale sale = new Sale();
        sale.setQuantity(saleDto.getQuantity());
        sale.setProduct(product);
        if (saleDto.getDiscount() != null) {
            sale.setDiscount(discountRepo.findOne(saleDto.getDiscount().getId()));
        }

        saleRepo.save(sale);
        return new ResponseEntity<>(SaleDto.convertEntityToDto(sale), HttpStatus.OK);
    }
}
