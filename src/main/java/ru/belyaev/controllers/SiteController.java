package ru.belyaev.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.belyaev.dto.DiscountInfoDto;
import ru.belyaev.dto.ProductDto;
import ru.belyaev.dto.SaleInfoDto;
import ru.belyaev.entities.Discount;
import ru.belyaev.entities.Product;
import ru.belyaev.entities.Sale;
import ru.belyaev.repositories.DiscountRepo;
import ru.belyaev.repositories.ProductRepo;
import ru.belyaev.repositories.SaleRepo;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by arkolit on 22.08.17.
 */
@Controller
public class SiteController {

    private final ProductRepo productRepo;

    private final SaleRepo saleRepo;

    private final DiscountRepo discountRepo;

    @Autowired
    public SiteController(ProductRepo productRepo, SaleRepo saleRepo, DiscountRepo discountRepo) {
        this.productRepo = productRepo;
        this.saleRepo = saleRepo;
        this.discountRepo = discountRepo;
    }

    @RequestMapping("/")
    public String index(Model model) {
        model.addAttribute("products", productRepo.findAll());
        return "index";
    }

    @RequestMapping("/sale_list/")
    public String sales(Model model){
        model.addAttribute("sales", SaleInfoDto.convertEntityToDto((Collection<Sale>) saleRepo.findAll()));
        model.addAttribute("productList", ProductDto.convertEntityToDto((Collection<Product>) productRepo.findAll()));
        return "sales";
    }

    @RequestMapping("/discount_list/")
    public String discounts (Model model) {
        model.addAttribute("discounts", DiscountInfoDto.convertEntityToDto((Collection<Discount>) discountRepo.findAll()));
        return "discounts";
    }
}
