package ru.belyaev.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.belyaev.dto.DiscountDto;
import ru.belyaev.entities.Discount;
import ru.belyaev.repositories.DiscountRepo;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by arkolit on 21.08.17.
 */
@RestController
@RequestMapping("/discount")
public class DiscountRestController {

    private DiscountRepo discountRepo;

    @Autowired
    public DiscountRestController(DiscountRepo discountRepo) {
        this.discountRepo = discountRepo;
    }

    @RequestMapping(method = RequestMethod.GET)
    public Collection<DiscountDto> readDiscount() {
        return DiscountDto.convertEntityToDto((Collection<Discount>) discountRepo.findAll());
    }
}
