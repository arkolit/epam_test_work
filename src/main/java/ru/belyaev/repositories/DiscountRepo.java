package ru.belyaev.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.belyaev.entities.Discount;

import java.util.Collection;

/**
 * Created by arkolit on 20.08.17.
 */

public interface DiscountRepo extends CrudRepository<Discount, Long> {
    public Collection<Discount> findDiscountsByIsActive(boolean isActive);
}
