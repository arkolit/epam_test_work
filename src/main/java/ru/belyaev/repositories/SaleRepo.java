package ru.belyaev.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.belyaev.entities.Product;
import ru.belyaev.entities.Sale;

import java.util.Collection;

/**
 * Created by arkolit on 15.08.17.
 */
@Repository
public interface SaleRepo extends CrudRepository<Sale, Long> {
}
