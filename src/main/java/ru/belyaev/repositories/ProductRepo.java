package ru.belyaev.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import ru.belyaev.entities.Product;

import java.util.Collection;

/**
 * Created by arkolit on 14.08.17.
 */
public interface ProductRepo extends CrudRepository<Product, Long> {

}
