package ru.belyaev.entities;

import javafx.geometry.Pos;
import lombok.*;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by arkolit on 14.08.17.
 */
@Data
@RequiredArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private long id;

    @NonNull
    private String name;

    @NonNull
    private double cost;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private Collection<Sale> sales;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @Where(clause = "is_active = true")
    private Collection<Discount> discounts;

}
