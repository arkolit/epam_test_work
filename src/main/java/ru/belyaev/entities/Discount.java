package ru.belyaev.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

/**
 * Created by arkolit on 20.08.17.
 */
@Data
@RequiredArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "discount")
public class Discount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private long id;

    @NonNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id")
    private Product product;

    @OneToMany(mappedBy = "discount", cascade = CascadeType.ALL)
    private Collection<Sale> sales;

    @NonNull
    private int amount;

    @Column(name = "date_discount")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateDiscount;

    @NonNull
    private boolean isActive;

    @PrePersist
    protected void onCreate() {
        dateDiscount = new Date();
    }

}
