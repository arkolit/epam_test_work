package ru.belyaev.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by arkolit on 23.08.17.
 */
@Data
@Entity
public class Sale {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(columnDefinition = "timestamp")
    private Date dateSale;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id")
    private Product product;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "discount_id", nullable = true, columnDefinition = "integer")
    private Discount discount;

    private int quantity;

    @PrePersist
    private void onCreate() {
        dateSale = new Date();
    }
}
