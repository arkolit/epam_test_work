package ru.belyaev.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.belyaev.entities.Discount;
import ru.belyaev.entities.Product;
import ru.belyaev.repositories.DiscountRepo;
import ru.belyaev.repositories.ProductRepo;

import java.util.Collection;
import java.util.List;
import java.util.Random;

/**
 * Created by arkolit on 20.08.17.
 */
@Component
public class DiscountTask {

    private final static int MIN_DISCOUNT = 5;
    private final static int MAX_DISCOUNT = 10;

    private Random random = new Random();

    private final ProductRepo productRepo;

    private final DiscountRepo discountRepo;

    @Autowired
    public DiscountTask(ProductRepo productRepo, DiscountRepo discountRepo) {
        this.productRepo = productRepo;
        this.discountRepo = discountRepo;
    }

    @Scheduled(cron="0 0 */1 * * ?")
    public void createNewDiscount() {
        List<Product> productList = (List<Product>) productRepo.findAll();
        if (!productList.isEmpty()) {
            int randomIndex = random.nextInt(productList.size());
            int randomDiscount = getRandomDiscount();

            Collection<Discount> discounts = discountRepo.findDiscountsByIsActive(true);
            for (Discount discount : discounts) {
                discount.setActive(false);
            }

            discountRepo.save(discounts);
            discountRepo.save(new Discount(productList.get(randomIndex), randomDiscount, true));
        }
    }

    private int getRandomDiscount () {
        return random.nextInt((MAX_DISCOUNT - MIN_DISCOUNT) + 1) + MIN_DISCOUNT;
    }
}
