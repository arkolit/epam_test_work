/**
 * Created by arkolit on 22.08.17.
 */
$(function () {
    $('#save-update').click(function (e) {
        var formValues = $('#saveform').serializeJSON();
        if (formValues.id === "") {
            $.ajax({
                url: '/products',
                type: "POST",
                data: JSON.stringify(formValues),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, textStatus, xhr) {
                    if (xhr.status === 200) {
                        location.reload();
                    }
                }
            });
        }
        else {
            $.ajax({
                url: '/products/' + formValues.id,
                type: "PUT",
                data: JSON.stringify(formValues),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, textStatus, xhr) {
                    if (xhr.status === 200) {
                        location.reload();
                    }
                }
            });
        }
    });

    $('.edit-item').click(function (e) {
        $.ajax({
            url: '/products/' + this.id,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, textStatus, xhr) {
                if (xhr.status === 200) {
                    $('input[name="id"]').val(data.id);
                    $('input[name="name"]').val(data.name);
                    $('input[name="cost"]').val(data.cost);
                }
            }
        });
    });

    if ($('#saveform-sale').prop('name') == 'save-form') {
        loadProductInfo($('#product').val());


        $('#product').change(function (e) {
            loadProductInfo($(this).val());
        });

        $('#add-position').click(function (e) {
            var formValues = $('#saveform-sale').serializeJSON();
            var newObj = {quantity: formValues.quantity, discount: {}};
            if (formValues.discount_id) {
                newObj.discount = {id:formValues.discount_id};
            }

            $.ajax({
                url: '/products/' + $('#product').val() + '/sale',
                type: "POST",
                data: JSON.stringify(newObj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, textStatus, xhr) {
                    if (xhr.status === 200) {
                        location.reload();
                    }
                }
            });
        });
    }
});

var loadProductInfo = function (productId) {
    $.ajax({
        url: '/products/' + productId,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, textStatus, xhr) {
            if (xhr.status === 200) {
                $('input[name="id"]').val(data.id);
                $('input[name="name"]').val(data.name);
                $('input[name="cost"]').val(data.cost);
                $('input[name="discount_id"]').val(data.discount ? data.discount.id : '');
                $('input[name="amount"]').val(data.discount ? data.discount.amount : '');
            }
        }
    });
};